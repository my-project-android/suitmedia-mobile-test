package com.suitmediamobiletest.api

import com.suitmediamobiletest.view.screen4.model.GuestModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BaseApiInterface {

    @GET("users")
    fun getUsers(@Query("page") page : String, @Query("per_page") perPage : String) : Call<WrappedListResponse<GuestModel>>

}