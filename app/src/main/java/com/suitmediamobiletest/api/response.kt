package com.suitmediamobiletest.api

import com.google.gson.annotations.SerializedName

data class WrappedResponse<T>(
    @SerializedName("status") var status : String,
    @SerializedName("message") var message : String,
    @SerializedName("data") var data : T
)

data class WrappedListResponse<T>(
    @SerializedName("page") var page: String,
    @SerializedName("per_page") var perPage : String,
    @SerializedName("total") var total : String,
    @SerializedName("total_pages") var totalPages : String,
    @SerializedName("data") var data : List<T>
)

data class AnotherResponse(
    @SerializedName("status") var status: String,
    @SerializedName("message") var message : String
)