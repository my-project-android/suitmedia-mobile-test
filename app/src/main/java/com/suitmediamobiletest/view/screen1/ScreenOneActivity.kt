package com.suitmediamobiletest.view.screen1

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.Gravity
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.suitmediamobiletest.R
import com.suitmediamobiletest.utils.CustomToast
import com.suitmediamobiletest.view.screen2.ScreenTwoActivity
import kotlinx.android.synthetic.main.activity_screen_one.*
import java.io.ByteArrayOutputStream

class ScreenOneActivity : AppCompatActivity() {
    //image pick code
    private val IMAGE_PICK_CODE = 1000;
    //Permission code
    private val PERMISSION_CODE = 1001;
    var resized : Bitmap? = null
    var CAMERA_REQUEST : Int? = 0
    var gambarKu : Bitmap? = null

    var strName : String? = ""
    var strPalindrome : String? = ""

    private var customToast = CustomToast()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_one)

        ivImageUserScreen1.setOnClickListener {
            dialogFoto()
        }

        cvCheckScreen1.setOnClickListener {
            if (!checkPalindromeText()) {

            } else {
                if (ispalindrome(strPalindrome!!)) {
                    customToast.customToast(this, "This is Palindrome")
                } else {
                    customToast.customToast(this, "This is Not Palindrome")
                }
            }
        }

        cvNextScreen1.setOnClickListener {
            if (!checkName() || !checkPalindromeText()) {

            } else {
                if (ispalindrome(strPalindrome!!)) {
                    var sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE)
                    var editor = sharedPreferences.edit()
                    editor.putString("username", strName)
                    editor.putString("palindrome", strPalindrome)
                    editor.putString("image", convertImageToStringForServer(gambarKu))
                    editor.commit()

                    startActivity(Intent(this, ScreenTwoActivity::class.java))
                    finish()
                } else {
                    customToast.customToast(this, "This is Not Palindrome")
                }
            }
        }
    }

    fun dialogFoto(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_open_photo)

        val window = dialog!!.window
        window!!.setGravity(Gravity.BOTTOM)
        window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        window!!.setBackgroundDrawable(resources.getDrawable(R.drawable.background_transparent))

        val openKamera = dialog.findViewById<TextView>(R.id.tvJudulAmbilFotoDialogPhoto)
        val openGallery = dialog.findViewById<TextView>(R.id.tvJudulUploadGambarDialogPhoto)

        openKamera.setOnClickListener {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST!!)
        }

        openGallery.setOnClickListener {
            openImageGallery()
        }

        dialog.show()
    }

    fun openImageGallery(){
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    //handle requested permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size >0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup granted
                    openImageGallery()
                }
                else{
                    //permission from popup denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    //untuk mengambil gambar hasil capture camera tadi kita harus override onActivityResult dan membaca resultCode apakah sukses dan requestCode apakah dari Camera_Request
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            val mphoto = (data!!.extras!!["data"] as Bitmap?)!!
            //panggil method uploadImage
            resized = Bitmap.createScaledBitmap(mphoto, 480, 640, true)
            var outputStream = ByteArrayOutputStream()
            resized?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            ivImageUserScreen1.setImageBitmap(resized)
            gambarKu = resized
        }

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            val photo : Bitmap = MediaStore.Images.Media.getBitmap(this!!.contentResolver, data?.data)
            resized = Bitmap.createScaledBitmap(photo, 480, 640, true)
            var outputStream = ByteArrayOutputStream()
            resized?.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            ivImageUserScreen1.setImageBitmap(resized)
            gambarKu = resized
        }
    }

    fun convertImageToStringForServer(imageBitmap: Bitmap?): String? {
        val stream = ByteArrayOutputStream()
        return if (imageBitmap != null) {
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            val byteArray: ByteArray = stream.toByteArray()
            Base64.encodeToString(byteArray, Base64.DEFAULT)
        } else {
            null
        }
    }

    private fun ispalindrome(text: String): Boolean {
        val reverseString = text.reversed().toString()
        return text.equals(reverseString, ignoreCase = true)
    }

    fun checkName() : Boolean {
        strName = edtUsernameScreen1.text.toString()

        if (strName!!.isEmpty()) {
            edtUsernameScreen1.setError("Can't be empty!")
            return false
        }

        return true
    }

    fun checkPalindromeText() : Boolean {
        strPalindrome = edtPalindromScreen1.text.toString()

        if (strPalindrome!!.isEmpty()) {
            edtPalindromScreen1.setError("Can't be empty!")
            return false
        }

        return true
    }
}