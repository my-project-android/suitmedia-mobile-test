package com.suitmediamobiletest.view.screen2

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.suitmediamobiletest.R
import com.suitmediamobiletest.view.screen3.ScreenThreeActivity
import com.suitmediamobiletest.view.screen4.ScreenFourActivity
import kotlinx.android.synthetic.main.activity_screen_two.*

class ScreenTwoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_two)

        var sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE)
        var username = sharedPreferences.getString("username", "")

        tvHelloUsernameScreen2.text = username

        cvChooseEventScreen2.setOnClickListener {
            startActivity(Intent(this, ScreenThreeActivity::class.java))
        }

        cvChooseGuestScreen2.setOnClickListener {
            startActivity(Intent(this, ScreenFourActivity::class.java))
        }
    }
}