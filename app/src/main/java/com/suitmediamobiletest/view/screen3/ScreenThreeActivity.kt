package com.suitmediamobiletest.view.screen3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.suitmediamobiletest.R
import com.suitmediamobiletest.view.screen3.adapter.AdapterEvents
import com.suitmediamobiletest.view.screen3.model.EventModel
import com.suitmediamobiletest.view.screen5.ScreenFiveActivity
import kotlinx.android.synthetic.main.activity_screen_three.*

class ScreenThreeActivity : AppCompatActivity() {

    var listEvents = mutableListOf<EventModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_three)

        listEvents.clear()
        listEvents.add(EventModel("1", "Opening Grand Mall", "Pembukaan peresmian Grand Mall", "12 Jan 2022", "9:AM"))
        listEvents.add(EventModel("2", "Opening Grand Mall", "Pembukaan peresmian Grand Mall", "12 Jan 2022", "9:AM"))
        listEvents.add(EventModel("3", "Opening Grand Mall", "Pembukaan peresmian Grand Mall", "12 Jan 2022", "9:AM"))
        listEvents.add(EventModel("4", "Opening Grand Mall", "Pembukaan peresmian Grand Mall", "12 Jan 2022", "9:AM"))
        listEvents.add(EventModel("5", "Opening Grand Mall", "Pembukaan peresmian Grand Mall", "12 Jan 2022", "9:AM"))

        rvListEventScreen2.apply {
            rvListEventScreen2.layoutManager = LinearLayoutManager(this@ScreenThreeActivity)
            rvListEventScreen2.adapter = AdapterEvents(listEvents)
        }

        ivMapScreen3.setOnClickListener {
            startActivity(Intent(this, ScreenFiveActivity::class.java))
        }

        ivBackScreen3.setOnClickListener {
            this.onBackPressed()
        }
    }
}