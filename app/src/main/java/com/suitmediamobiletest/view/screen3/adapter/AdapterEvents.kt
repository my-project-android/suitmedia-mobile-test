package com.suitmediamobiletest.view.screen3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.suitmediamobiletest.R
import com.suitmediamobiletest.view.screen3.model.EventModel
import com.suitmediamobiletest.view.screen4.adapter.AdapterGuest
import kotlinx.android.synthetic.main.inflater_events.view.*

class AdapterEvents(private val itemList : MutableList<EventModel>) : RecyclerView.Adapter<AdapterEvents.EventsViewHolder>() {

    private lateinit var context: Context

    class EventsViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.inflater_events, parent, false)
        val ksv = EventsViewHolder(view)
        context = parent.context
        return ksv
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        holder.itemView.tvItemEventsTitle.text = itemList[position].eventTitle
        holder.itemView.tvItemEventsDesc.text = itemList[position].eventDesc
        holder.itemView.tvItemEventsDate.text = itemList[position].eventDate
        holder.itemView.tvItemEventsTime.text = itemList[position].eventTime
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}