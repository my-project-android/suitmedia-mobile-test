package com.suitmediamobiletest.view.screen3.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventModel(
    var idEvents : String?,
    var eventTitle : String?,
    var eventDesc : String?,
    var eventDate : String?,
    var eventTime : String?
) : Parcelable {
    constructor() : this(null, null, null, null, null)
}