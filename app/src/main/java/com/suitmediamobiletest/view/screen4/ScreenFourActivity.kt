package com.suitmediamobiletest.view.screen4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.friendme.utils.CustomProgressDialog
import com.suitmediamobiletest.R
import com.suitmediamobiletest.utils.CustomToast
import com.suitmediamobiletest.view.screen4.adapter.AdapterGuest
import com.suitmediamobiletest.viewmodel.ScreenFourVM
import kotlinx.android.synthetic.main.activity_screen_four.*
import kotlinx.android.synthetic.main.activity_screen_three.*

class ScreenFourActivity : AppCompatActivity() {

    var screenFourVM : ScreenFourVM? = null
    private var customProgressDialog = CustomProgressDialog()
    private var customToast = CustomToast()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_four)

        screenFourVM = ScreenFourVM()
        screenFourVM?.getGuest("1", "10")

        initLive()

        ivBackScreen4.setOnClickListener {
            this.onBackPressed()
        }
    }

    fun initLive() {
        screenFourVM?.onIsLoading()!!.observe(this) {
            if (it) {
                customProgressDialog.show(this, "Synchronized Data!")
            } else {
                customProgressDialog.dialog.dismiss()
            }
        }

        screenFourVM?.onIsMessage()!!.observe(this) {
            customToast.customToast(this, it)
        }

        screenFourVM?.onGetUser()!!.observe(this) {
            rvListGuestScreen4!!.apply {
                rvListGuestScreen4.layoutManager = GridLayoutManager(this@ScreenFourActivity, 2)
                rvListGuestScreen4.adapter = AdapterGuest(it)
            }
        }
    }
}