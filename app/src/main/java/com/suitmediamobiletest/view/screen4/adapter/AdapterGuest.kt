package com.suitmediamobiletest.view.screen4.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.suitmediamobiletest.R
import com.suitmediamobiletest.view.screen4.model.GuestModel
import kotlinx.android.synthetic.main.inflater_guest.view.*

class AdapterGuest(private val itemList : MutableList<GuestModel>) : RecyclerView.Adapter<AdapterGuest.GuestViewHolder>() {

    private lateinit var context: Context

    class GuestViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.inflater_guest, parent, false)
        val ksv = GuestViewHolder(view)
        context = parent.context
        return ksv
    }

    override fun onBindViewHolder(holder: GuestViewHolder, position: Int) {
        Glide.with(context).load(itemList[position].avatarGuest).into(holder.itemView.ivItemGuest)
        holder.itemView.tvItemNameGuest.text = itemList[position].firstName + itemList[position].lastName
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}