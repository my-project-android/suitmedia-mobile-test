package com.suitmediamobiletest.view.screen4.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GuestModel(
    @SerializedName("id") var idGuest : String?,
    @SerializedName("email") var emailGuest : String?,
    @SerializedName("first_name") var firstName : String?,
    @SerializedName("last_name") var lastName : String?,
    @SerializedName("avatar") var avatarGuest : String?
) : Parcelable {
    constructor() : this(null, null, null, null, null)
}