package com.suitmediamobiletest.view.screen5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.suitmediamobiletest.R
import com.suitmediamobiletest.databinding.ActivityScreenFiveBinding
import com.suitmediamobiletest.view.screen3.ScreenThreeActivity
import kotlinx.android.synthetic.main.activity_screen_five.*

class ScreenFiveActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityScreenFiveBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScreenFiveBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        ivListItemScreen5.setOnClickListener {
            startActivity(Intent(this, ScreenThreeActivity::class.java))
            finish()
        }

        ivBackScreen5.setOnClickListener {
            this.onBackPressed()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val center = CameraUpdateFactory.newLatLng(LatLng(0.0, 0.0))
        val zoom = CameraUpdateFactory.zoomTo(15f)

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(0.0, 0.0)
        mMap.addMarker(MarkerOptions().position(sydney).title(""))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        mMap.moveCamera(center)
        mMap.animateCamera(zoom)
    }
}