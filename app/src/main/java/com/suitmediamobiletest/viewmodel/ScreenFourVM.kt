package com.suitmediamobiletest.viewmodel

import androidx.lifecycle.MutableLiveData
import com.suitmediamobiletest.api.ApiClient
import com.suitmediamobiletest.api.WrappedListResponse
import com.suitmediamobiletest.view.screen4.model.GuestModel
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ScreenFourVM {

    private var apiService = ApiClient.APIService()
    private var isLoading : MutableLiveData<Boolean> = MutableLiveData()
    private var strMessage : MutableLiveData<String> = MutableLiveData()
    private var listGuest = mutableListOf<GuestModel>()
    private var getListGuest : MutableLiveData<MutableList<GuestModel>> = MutableLiveData()

    fun getGuest(page : String, perPage : String) {
        val request = apiService.getUsers(page, perPage)
        isLoading.value = true
        request.enqueue(object : Callback<WrappedListResponse<GuestModel>> {
            override fun onFailure(call: Call<WrappedListResponse<GuestModel>>, t: Throwable) {
                isLoading.value = false
                strMessage.value = t.message
            }

            override fun onResponse(
                call: Call<WrappedListResponse<GuestModel>>,
                response: Response<WrappedListResponse<GuestModel>>
            ) {
                if (response.isSuccessful) {
                    try {
                        isLoading.value = false

                        var body = response.body()
                        for (i in 0 until body!!.data.size) {
                            var data = body.data[i]
                            listGuest.add(GuestModel(data.idGuest, data.emailGuest, data.firstName, data.lastName, data.avatarGuest))
                        }

                        getListGuest.value = listGuest
                    } catch (e : JSONException) {
                        e.printStackTrace()
                        isLoading.value = false
                    }
                }
            }
        })
    }

    fun onIsLoading() : MutableLiveData<Boolean> {
        return isLoading
    }

    fun onIsMessage() : MutableLiveData<String> {
        return strMessage
    }

    fun onGetUser() : MutableLiveData<MutableList<GuestModel>> {
        return getListGuest
    }
}